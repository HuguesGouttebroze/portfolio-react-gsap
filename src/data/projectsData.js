/* import frog1 from '../assets/frog1.png'
import conceptionSiteFroggit from '../assets/conceptionSiteFroggit.png'
import mollyHome from '../assets/mollyHome.png' */

export const projectsData = [
  {
    id: 1,
    title: "Froggit",
    client: "Lydra, a DevOps compagnie from Saint-Etienne, France",
    date: "Octobre 2020 - Octobre 2022",
    languages: ["React", "Gatsby.js", "GitLab CI", "Docusaurus", "CSS in JS / Styled Components", "git", "Docker", "Insckape, an open source software to work on SVG"],
    work: ["AGILE / SCRUM", "DevOps"],
    principes: ["Code Revues", "Pair Programming", "Clean code"],
    infos:
      "I've been working on Froggit software design & developpement. I have also work on Froggit logo conception & Froggit web documentation.",
/*     img: conceptionSiteFroggit,
 */    link: "http://www.google.com",
    infosFr: "J'ai travaillé pour Lydra dans le cadre de la conception et du développement de Froggit, logiciel en ligne de sauvegarde & partage de code, une forge logiciel type Github, Gitlab mais hébergé en france & à ce titre non soumis au Cloud Act."
  },
  {
    id: 2,
    title: "Molly",
    client: "Consultant Développeur JavaScript FullStack | DevoTeam",
    date: "Janvier 2021 - Octobre 2021",
    languages: ["React", "Gatsby.js", "GitLab CI", "Docusaurus", "CSS in JS / Styled Components", "git", "Docker", "Insckape, an open source software to work on SVG"],
    work: ["AGILE / SCRUM", "DevOps"],
    principes: ["Code Revues", "Pair Programming", "Clean code"],
    infos:
      "",
/*     img: mollyHome,
 */    link: "",
  },
  {
    id: 3,
    title: "refonte des formulaires",
    client: "Advercity",
    date: "Novembre 2022 - Janvier 2023",
    languages: ["React", "RxJS", "MUI", "AWS", "PHP/Laravel/Symfony", "MySQL", "ElasticSearch", "git", "Docker", ""],
    work: ["AGILE", "DevOps"],
    principes: ["Code Revues", "Pair Programming", "Clean code"],
    infos:
      "sur des applications / site à fort trafic, passage d'une architecture monolitique vers une architecture divisée en micro-services, implémentation de formulaires dynamiques, réutilisables & génériques",
/*     img: conceptionSiteFroggit,
 */    link: "http://www.google.com",
  },
  {
    id: 4,
    title: "Creative Dev",
    client: "Lydra, a DevOps compagnie from Saint-Etienne, France",
    date: "Octobre 2020 - Octobre 2022",
    languages: ["React", "Gatsby.js", "GitLab CI", "Docusaurus", "CSS in JS / Styled Components", "git", "Docker", "Insckape, an open source software to work on SVG"],
    work: ["AGILE / SCRUM", "DevOps"],
    principes: ["Code Revues", "Pair Programming", "Clean code"],
    infos:
      "I've been working on Froggit software design & developpement. I have also work on Froggit logo conception & Froggit web documentation.",
/*     img: conceptionSiteFroggit,
 */    link: "http://www.google.com",
  },
];

export const passionsData = [
  {
    id: 1,
    title: "retro gaming",
    description: "Ayant grandi dans les années 80, j'ai eu la chance de pouvoir utilisé des AMIGA, notement l'AMIGA 500 puis l'AMIGA 1200. J'adorais les jeux vidéos qui tournaient dessus, tel que SPEED-BALL, RICK DANGEROUS ou encore MARBLE MADNESS. Aujourd'hui, j'aime programmer des jeux qui reprennent les mêmes principes, des graphismes ultra simples ... Voici quelques jeux vidéos que j'ai programmé",
    liste: ["Space Invader", "car game", "puissance 4"]
  }
]

export const personalProjects = [
  {
    id: 1,
    title: 'Mavel',
    link: 'https://marvel-hg-project.vercel.app/'
  }
]

export const softSkills = [
  {id: 1, title: 'Créativité'}, 
  {id: 2, title: 'Curiosité'}, 
  {id: 3, title: 'Passion'}, 
  {id: 4, title: 'Bienveillance'}, 
  {id: 5, title: 'Engagement'}, 
  {id: 6, title: 'Ténacité'}, 
  {id: 7, title: 'Travail en équipe'}, 
  {id: 8, title: 'Autonomie'}, 
  {id: 9, title: 'Partage'}, 
  {id: 10, title: 'Proactivité'},
  {id: 11, title: 'Ampathie'}
]

export const hardSkills = [
  "Conception et Développement d'applications  (MongoDB, Express, Angular ou React, Node.js)",
  "Programmation MULTI PARADIGME",
  "Orientée Objet",
  "Fonctionnelle",
  "Reactive",
  "Design patterns ",     
  "Principes SOLID",
  "CLEAN CODE",
  "CODE REVIEWS",
  "PAIR PROGRAMMING",
  "Methodo. AGILE, Pratique & culture DevOps",

"Contributions projets OPEN SOURCE",

{front: "React, NextJS, Angular, NgRX, RxJS, Redux Pattern"},

{back: "Node.js, NestJS, Express, Fastify, Electron, WebSockets, JWT"},

"JavaScript, TypeScript, ECMAScript6+",

"MySQL, MariaDB, MongoDB, ElasticSearch",

"Tests unitaires & fonctionnelles",

"CI/CD",
"DOCKER",
"GIT",

"API ReST - GraphQL",

"VIM, LINUX, SHELL, Git",
"HTML/CSS, maquettage, SCSS, Desin Systems, SVG, Accessibilité, SEO ",

"jeux  vidéo en JavaScript (Canvas - 2d)",
"Animations/Transitions CSS/JS, librairie GSAP, Canvas, Animations, SVG, Scroll & Parrallaxe"
]

export const openSourceProjects = [
  {
    id: 1,
    title: "template on showcase Docusaurus React SSG",
    technos: "React",
    link: ""
  },
  {
    id: 2,
    project: 'Gitlab pages',
    title: "Gitlab CI",
    technos: "React",
    link: ""
  }
]

export const techSkills = ["JavaScript", "JAVA", "PHP", "SQL/noSQL", "Linux/Unix", "Conception Orientée Objet", "programmation fonctionnelle", "programmation réactive", "Gestion de projets", "SCRUM", "DevOps", "principes SOLID"]
