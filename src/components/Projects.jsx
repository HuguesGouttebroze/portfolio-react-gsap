import React, { useEffect } from 'react'
import { projectsData } from '../data/projectsData'
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";
import { ScrollToPlugin } from "gsap/ScrollToPlugin";
import { TextPlugin } from "gsap/TextPlugin";
import { useRef } from 'react';

gsap.registerPlugin(ScrollTrigger, ScrollToPlugin, TextPlugin);


export const Projects = () => {
    const titleRef = useRef()
    const onEnter = ({currentTarget}) => {
        gsap.to(currentTarget, {backgroundColor: 'orange', color: 'black'})
    }
    const onLeave = ({currentTarget}) => {
        gsap.to(currentTarget, {backgroundColor: 'black', color: 'white'})
    }
    const onLoad = () => {
        gsap.timeline().fromTo('.letter', 
            {
                x: -100,
                opacity: 0
            },
            {
                x: 0,
                opacity: 1,
                stagger: 0.33,
                delay: 0.7
            },
        ).to('.title', {
           y: 45,
           delay: 0.7 
        }).to('.letter', {
            margin: '0.5vw', 
            delay: 0.8,
            duration: 0.5
        }).to('.letter', {
            x: -titleRef.current.clientWidth,
            delay: 1,
            duration: 2,
            rotate: -360
        }).to(window, {
            duration: 0.5,
            scrollTo: '.first'
        }).to('.title', {
            y: 0
        }).to('.letter', {
            x:0,
            delay: 1,
            duration: 2
            
        })
    }

    useEffect(() => {
        onLoad()
    })
    const slideToTop = (elem, delay, duration) => {
        gsap.fromTo(
            elem,
            {
                opacity: 0,
                y: -200
            }, 
            {
                opacity: 1,
                y: 0,
                stagger: 0.33,
                
                scrollTrigger: {
                    trigger: elem,
                    start: 'top center',
                    end: 'bottom center',
                }
            }
        )
    }
    const slideLeft = (elem, delay, duration) => {
        gsap.fromTo(
            elem,
            {
                opacity: 0,
                x: -200
            }, 
            {
                opacity: 1,
                x: 0,
                stagger: 0.33,
                scrollTrigger: {
                    trigger: elem,
                    start: 'top center',
                    end: 'bottom center',
                }
            }
        )
    }

    useEffect(() => {
        slideToTop('.first')
    }, [])

    useEffect(() => {
        slideLeft('.first')
    }, [])
  return (
    
        <div className=''>
            <h3 className='' ref={titleRef}>
                <span className='letter'>P</span>
                <span className='letter'>r</span>
                <span className='letter'>o</span>
                <span className='letter'>j</span>
                <span className='letter'>e</span>
                <span className='letter'>t</span>
                <span className='letter'>- </span>
                <span className='letter'>P</span>
                <span className='letter'>r</span>
                <span className='letter'>o</span>
                <span className='letter'>.</span>
            </h3>
            <div>
                {projectsData.map(({id, title, img, 
                                    client, date, infos, 
                                    languages, principes, 
                                    work, link
                                    }) => (
                    <div className='first' 
                        key={id} 
                        onMouseEnter={onEnter}
                        onMouseLeave={onLeave}
                    >
                        {title}
                        <img src={img} alt={title} className='w-1/2' />
                        <h3 className=''>{client} - {date}</h3>
                        <p>
                            Quelques mots sur le projets ... 
                            <br />
                            {infos}
                        </p>
                        <p>Principales technos utilisées:{languages}</p>
                        <br/>
                        <p className='text-xl'>
                            Méthodologies de travail, principes de collaboration, de travail & de programmation:
                            {principes}, {work}
                        </p>
                        <a href={link}
                            target="_blank"
                            rel="noreferrer"
                        >
                            ...en savoir d'avantage?
                        </a>
                    </div>
                ))

                }
            </div>
        </div>
    )
}
