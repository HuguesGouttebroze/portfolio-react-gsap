import React,{ useEffect, useRef, useState, useImperativeHandle, forwardRef } from 'react'
import { gsap } from "gsap";

export const Circle = forwardRef(({ size, delay, children }, ref) => {
  const el = useRef();
    
  useImperativeHandle(ref, () => {           
    
    // return our API
    return {
      moveTo(x, y) {
        gsap.to(el.current, { x, y, delay });
      }
    };
  }, [delay]);
  
  return (
    <div className={`circle ${size}`} ref={el}>{children}</div>
    );
});
