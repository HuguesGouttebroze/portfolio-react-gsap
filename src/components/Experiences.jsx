import React from 'react'

export const Experiences = () => {
  return (
    <div>
        <svg id="svg" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 600 1200">
            <path className="line01 line" d="M 10 200  600 200" ></path>
            <path className="line02 line" d="M 10 400  600 400" ></path>
            <path className="line03 line" d="M 10 600  600 600" ></path>
            <path className="line04 line" d="M 10 800  600 800" ></path>
            <path className="line05 line" d="M 10 1000  600 1000" ></path>
            <text fill='white' className="text01" x="30" y="190">2020 - Developer Front-End REACT</text>
            <text fill='white' className="text02" x="30" y="390">2021 - Engineer in development graduated</text>
            <text fill='white' className="text03" x="30" y="590">2022 - Consultant dev. fullstack</text>
            <path className="theLine" 
                    d="M -5,0
                    Q 450 230 300 450 
                    T 130 750
                    Q 100 850 300 1000
                    T 150 1200"
                    fill="none" stroke="white" stroke-width="10px" />
            <circle fill='orange' className="ball ball01" r="20" cx="50" cy="100"></circle>
            <circle fill='orange' className="ball ball02" r="20" cx="278" cy="201"></circle>
            <circle fill='orange' className="ball ball03" r="20" cx="327" cy="401"></circle>
            <circle fill='orange' className="ball ball04" r="20" cx="203" cy="601"></circle>
        </svg>
    </div>
  )
}
